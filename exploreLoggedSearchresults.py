# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 02:21:32 2020

@author: danie
"""
import json

searches=[]
with open('searchExplorer.log','r') as f:
    searches=json.load(f)
    

# make dict of all successful searches (no error string)
d=dict()
for el in searches:
    if type(el[2]) is str:
        pass
    else:
        d[el[1]]=el[2]
        

# now check what we found. eg list of resultcounts:
for k,v in d:
    print(k,v['searchInformation']['formattedTotalResults'])
    