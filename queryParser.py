# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 19:03:09 2020

@author: danie
"""

##### before first use run once: 
# import nltk
# nltk.download('stopwords')
#####

import re
import random as rnd
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
stopWords = set(stopwords.words('english'))

rnd.seed(42)

known_operators = ('intext:','inurl:','intitle:','define:', 'translate:')

# every function in this module should take a string as the only required argument and can take optional keyword args that alter behavioour.
# every function returns an output string and a dictionary, containing at least the pair 'report':'<some string>'

def naiveParser(instring):
    instring=instring.lower()
    patterns=[r'[^\<\>]?(\".*?\")[^\<\>]', r'(\([^\<\>]*?\))',r'([^\<\>]*?)[\s$]',r'\b([a-z]*)\b']
    querySplit=[]
    for pattern in patterns:
        print('pattern: ',pattern)
        if instring.strip()!="":
            matches=re.findall(pattern,instring)
            for match in matches:
                if match!="":
                    print('match: ',match)
                    instring=instring.replace(match,"<{}>".format(len(querySplit))).strip()
                    querySplit.append(match)
    # now get rid of all trash characters between the position tags:
    trash=re.findall(r'\>(\S*?)\<',instring)
    for el in trash:
        instring=instring.replace(el," ")
    return querySplit, instring
    
def naiveAntiParser(searchTerms,posString):
    cursorInTag = False
    cache = ''
    res = ''
    for el in posString:
        if not cursorInTag and el!='<':
            res+=el
        elif not cursorInTag and el=='<':
            cursorInTag=True
        elif cursorInTag and el!='>':
            cache+=el
        elif cursorInTag and el=='>':
            res += searchTerms[int(cache)]
            cursorInTag = False
            cache = ''
        else:
            raise Exception('Error occured parsing search terms to query string:\n\t{}'.format(str((searchTerms,posString,res))))
    return res


def dropTerm(searchTerms,posString):
    dropIndex = rnd.randint(0,len(searchTerms))
    try:
        searchTerms[dropIndex]=""
        ret=(searchTerms,posString,True)
    except:
        ret=(searchTerms,posString,False)
    return ret

def swapQuotes(searchTerms,posString):
    return searchTerms,posString,False
    

def modifyQuery(searchTerms,posString, maxretries=None):
    """ randomly picks a modification and tries to perform it on the inputs. 
    returns result on success, retries with a new pick on fail (caution:danger of infinite Loop...)"""
    counter = 0
    modSuccess=False
    mods = [dropTerm, swapQuotes]
    while counter != maxretries and not modSuccess:
        counter += 1
        mod=rnd.choice(mods)
        s,p,modSuccess = mod(searchTerms,posString)
    return s,p

def findStopwords(instring):
    words = word_tokenize(instring)
    wordsFiltered = []
    wordsRemoved = set()
    for word in words:
        if word not in stopWords:
            wordsFiltered.append(word)
        else: 
            wordsRemoved.add(word)
    report = 'removed {0} stopwords from query string'.format(str(len(wordsRemoved)))
    outstring = ' '.join(wordsFiltered)
    return (outstring,{'report':report,'removed':wordsRemoved})


def protectDFphrases(instring, entities, report=None):
    """ checks for each entity-value in entities (ignoring '@sys.all') and removes it from instring.
    returns the remaining string and a list of df-phrases alogn with the report """
    addReport = ''
    df_phrases = []
    for entity_name,entity_value in entities.items():
        try: 
            instring=instring.remove(entity_value)
            df_phrases.append((entity_value,entity_name))
        except Exception as e:
            addReport.append(entity_value,str(e))
    addReport+='Removed {} matching phrases for dialogflow-entities'.format(len(df_phrases))
    if report:
        report += addReport
    else: 
        report = addReport
    return (instring,{'report':report,'df_phrases':df_phrases})



def findOperators(instring, report=None):
    wordList = instring.split()
    foundOperators=[]
    for word in wordList:
        if word in known_operators:
            operand = getOperand(wordList[wordList.index(word):])
            foundOperators.append((operand,word))
    addReport = ''
    if report:
        report += addReport
    else: 
        report = addReport
    return (instring, {'report':report,'foundOperators':foundOperators})
    
def getOperand(inList):
    ret=''
    if not inList[0].strip().startswith('"'):
        ret=(inList[0].strip())
    else:
        s = ' '.join(inList)
        ret = re.match(pattern='\"(.*)\"', string=s)
    return ret
        
#### How do we parse a query string?
# Let's consider a few use cases and what should happen:
#    - input contains quotes or other known_operators that the user put in
#        ==> quotations and operands become special phrase tuples (operator,operands)
#    - input is a veryvery long text (>10 short phrases or words)
#        ==> user probably wants an exact match (looking for literature/citation/...?), sitesearch (looking for error message on stackoverflow....?), ...
#        ==> might benefit from distance operator & wildcards for variable/uncertain parts of input
#        =====> other than that, don't touch input string, say "sorry, can't help you" and exit
#    - input is a single keyword found in google trends (eg. "coronavirus", or "coronavirus hessen")    
#        ==> looking for news? (date/time search parameters?)
#        ==> looking for localized info?
#    - input is a single phrase not found in google trends
#        ==> looking for definition?
#    - input contains (part of) a url or service
#        ==> site restricted search?
#    - input contains a mix of phrases, unknown words, unrecognized punctuation, etc....
#        ==> this is the tough one.... what do?


#### After parsing the string:
#    ==> end up with a list of (phrase,operator)-tuples and non-phrase text (<10 entries)
#    examples: [("steve jobs","quo"),"fired","by", ("apple inc.","quo")]
#    ==> reassemble query string (apply operands and merge)
#    ==> randomly modify operands to obtain new variantions of input query
#    ==> perform a few searches and analyse results maybe? how? 
#        how do we know if we're getting closer to what user wants?

##### Analyse results:
#    =====> check for corrected query string, ask user validation
#    ==> check domain names
#    ==> categorize results somehow (metatags?dbpedia?semanticWeb-magic?)
#    ==> get entire page or just snippets from resultlist and check for occurences in search Terms