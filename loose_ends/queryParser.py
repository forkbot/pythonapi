#! /usr/bin/python3
### Import section
try:
    import json
    from flask import Flask, request, make_response, render_template, session, flash
except Exception as e:
    print("Import error occured, please check dependencies! \n{}".format(e))
### End Import section
        
### init flask in global layout
app = Flask(__name__)

req_params = {'query': 'user input query string, required', 'entities': '{"@dictionary.of": "dialogflow entities", "@with.values": "occuring in query string"}'}

known_entities = ['@sys.location', '@sys.number', '@sys.person']

usage = 'usage: expects json-post with params {}'.format(str(req_params.keys()))

@app.route('/webhook', methods=['POST','GET'])
def webhook():
    if request.method == "POST":
        req = request.get_json(silent=True, force=True)
        res = processRequest(req)
    else:
        res = usage
    res = json.dumps(res,indent=4)
    r = make_response(res)
    r.headers['Content-Type'] = 'application/json'
    return r

def processRequest(req):
    if not req.query:
        return list(usage,' ::: no query ::: ')
    query_phrases=[]
    unknown_entities
    if req.entities:
        for entitiy,val in req.entities:
            if entity not in known_entities:
                unknown_entities.append(entity)
            else:
                query_phrases.append(val)
    ret = list(usage, ' ::: unknown entities ::: ',unknown_entities,' ::: query ::: ',' '.join(query_phrases))
    return ret






if __name__ == "__main__":
    app.run(debug=True)
