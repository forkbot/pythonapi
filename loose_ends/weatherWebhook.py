# -*- coding: utf-8 -*-
"""
webhook for use in dialogflow-fulfillment (agent:"testBot",intent:"weatherReport")

@author: danie
"""

### Import section
try:
    import json
    from flask import Flask, request, make_response
except Exception as e:
    print("Import error occured, please check dependencies! \n{}".format(e))
### End Import section
    
### init flask in global layout
app = Flask(__name__)

@app.route('/webhook', methods=['POST','GET'])
def webhook():
    if request.method == "POST":
        
        req = request.get_json(silent=True, force=True)
        res = processRequest(req)
    else:
        res = 'falsch, bruder. gibts keine diese.'
    res = json.dumps(res,indent=4)
    r = make_response(res)
    r.headers['Content-Type'] = 'application/json'
    return r

def processRequest(rqst):
    query_parameters = rqst["queryResult"]
    text = query_parameters.get('queryText', None)
    params = query_parameters.get('parameters', None)
    print(text, params)
    res = get_data(text,params)
    return res

def get_data(txt,para):
    antwort = "is noch nich implementiert, alter. keine zeit.\n aber du wolltest dings hier, oder?\n"
    return {"fulfillmentText": antwort+txt+str(para)}

if __name__ == "__main__":
    app.run(debug=True)
