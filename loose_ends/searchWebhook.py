# -*- coding: utf-8 -*-
"""
!!!! Doku hier: https://cloud.google.com/dialogflow/docs/fulfillment-webhook#webhook_response
webhook for use in dialogflow-fulfillment (agent:"testBot",intent:"weatherReport")

@author: danie
"""

### Import section
try:
    import json
    from flask import Flask, request, make_response
    from googleapiclient.discovery import build
except Exception as e:
    print("Import error occured, please check dependencies! \n{}".format(e))
### End Import section
    
### init flask in global layout
app = Flask(__name__)

### init cse-resource
api_key = "AIzaSyBbtU8iAGoYVhWtv4irXn28W5CcJIEZzB8"
cse_key = "007175631489998748943:w29zkedhcri"


resource = build("customsearch", "v1", developerKey=api_key).cse()

@app.route('/search', methods=['POST'])
def search():
    try:
        query = getQuery()
        results = doSearch(query)
    except Exception as e:
        results = 'error occured:\n' + str(type(e)) + str(e)
    
    response = wrapResults(results)
    return response

def getQuery():
    if request.method == "POST":
        req_body = request.get_json(silent=True, force=True)
        try:
            query = req_body['query']
        except ValueError as e:
            raise Exception('no query string')
    else:
        raise Exception('non-POST request somehow...?')
    return query

def doSearch(query_string):
    result = resource.list(q=query_string, cx=cse_key).execute()    
    return result   #'not implemented.\n'+str(q)

def wrapResults(res):
    res = json.dumps(res,indent=4)
    r = make_response(res)
    r.headers['Content-Type'] = 'application/json'
    return r

#def processRequest(rqst):
#    if rqst["userQuery"]:
#        pass
#    else: query_parameters = str(rqst)
#    text = query_parameters.get('queryText', None)
#    params = query_parameters.get('parameters', None)
#    res = get_data(text,params)
#    return res

#def get_data(txt,para):
#    antwort = "Webhook received: \n---------------\n"
#    return {"fulfillmentText": antwort+txt+str(para)}

if __name__ == "__main__":
    app.run(debug=True)
