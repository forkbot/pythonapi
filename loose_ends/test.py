#! /usr/bin/python3
import json,random
import cgi
#import cgitb
#cgitb.enable()


ret=dict()
print("Content-Type: application/json")     # JSON comes out
print()                                     # blank line, end of headers

try:
    params = cgi.FieldStorage()
    for el in params.keys():
        ret[el]=str(params.getvalue(el))
except:
    cgi.print_exception()

easter_eggs = ['askew','flip a coin','do a barrel roll','roll a die']
random.seed()
choice=random.choice(easter_eggs)
ret['query']=choice
ret=json.dumps(ret)
print(ret)

