# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 23:25:26 2020

@author: danie
"""

# import nltk
# nltk.download('words')

import json, requests
from nltk.corpus import words as wrds
#lst=list(wrds.words('en-basic'))

import queryParser

api_url = 'https://entty.eu/searchApp/search'

from pytrends.request import TrendReq

pytrends = TrendReq(hl='en-US', tz=360)
lst = set([ el for el in pytrends.trending_searches(pn='united_states').get(0)])
lst = lst.union(set([ el for el in pytrends.trending_searches(pn='germany').get(0)]))

with open('searchExplorer.log','a') as f:
    for counter,query in enumerate(lst):
        post_params = json.dumps({'query':query})
        try:
            res4 = requests.post(api_url, data=post_params)
            #spit(res4)
            res_dict=json.loads(str(res4.content,'utf-8'))
            log_msg = json.dumps([counter,query,res_dict],indent=4)
        except Exception as e:
            log_msg= json.dumps([counter,str(type(e))+str(e)],indent=4)
        f.write(log_msg)
        
        
#In [39]: res4.content
#Out[39]: b'"error occured:\\n<class \'googleapiclient.errors.HttpError\'>
#   <HttpError 429 when requesting https://customsearch.googleapis.com/customsearch/v1?alt=json&q=young&cx=007175631489998748943%3Aw29zkedhcri&key=AIzaSyBbtU8iAGoYVhWtv4irXn28W5CcJIEZzB8 
#   returned \\"Quota exceeded for quota metric \'Queries\' and limit \'Queries per day\' of service \'customsearch.googleapis.com\' 
#   for consumer \'project_number:794863591392\'.\\">"'
        
        
## try same with googlesearch-module from local blackbox:
#
#try: 
#	from googlesearch import search 
#except ImportError: 
#	print("No module named 'google' found") 
#
#with open('searchExplorer_piratyVersion.log','a') as f:
#    for counter, query in enumerate(lst):
#        lst=list(search(query, num=100, stop=None, pause=3.0))
#        print(query,len(lst))
#        log_msg = json.dumps([counter,query,lst])
#        f.write(log_msg)
#        
        
#I 221
#a 234
#able 257
#about 280
        
#--> HTTPError: Too Many Requests