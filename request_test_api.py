# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 21:05:51 2020

test api with http post/get requests
"""

import requests, json

## OLD cgi-version: test_url = 'http://entty.eu:201/hmiapi/test.py'
test_url = 'https://hmi.entty.eu/searchApp/search'  #/search'
get_params = {'query':'query string','bla':'blubbedi','dödel':'up'}
## OLD post_params = json.dumps(['list','of',3,'items',{'plus':'dict'},['plus','another','list']])
post_params = json.dumps({'query':'wer ist er, dieser steven jobs?', 'intent':'spielerei', 'liste':[1,2,3], 'dictum':{1:2,3:4}})

def spit(response):
    print('req method: ',response.request.method)
    print('req headers: ',response.request.headers)
    print('req body: ',response.request.body)
    print()
    print('res header: ',response.headers)
    print('res content: ',response.content)
    print('::: ::: :::\n')
    

## try get request withou params:
#try:
#    res1 = requests.get(test_url)
#    spit(res1)
#except Exception as e:
#    print(e)
#
### try get request with params:
#try:
#    res2 = requests.get(test_url,params=get_params)
#    spit(res2)
#except Exception as e:
#    print(e)
#
### try post request without params:
#try:
#    res3 = requests.post(test_url)
#    spit(res3)
#except Exception as e:
#    print(e)

## try post request without params:
try:
#    res4 = requests.get(test_url, params=get_params)
#    #spit(res4)
#    res4_data=json.loads(str(res4.content,'utf-8'))
#    print(res4_data)
    
    res5 = requests.post(test_url, json=json.dumps(get_params))
    #spit(res4)
    res5_data=json.loads(str(res5.content,'utf-8'))
    print(res5_data)
#    for k,v in res_dict.items():
#        print(k,str(v))
except Exception as e:
    print(e)