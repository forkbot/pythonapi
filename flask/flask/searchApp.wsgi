#! /usr/bin/python3

import logging

import sys
logging.basicConfig(stream=sys.stderr)
#logging.basicConfig(format='%(asctime)s :::%(levelname)s::: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',filename='loggingFromWSGI.log',level=logging.DEBUG)
sys.path.insert(0, '/var/www/hmiapi/flask/flask/')
#logging.debug('all systems functional')

try:
    from searchApp import app as application
    application.secret_key = 'thismakee'
except ImportError as e:
    #print('; '.join(sys.path))
    print(type(e),str(e))

