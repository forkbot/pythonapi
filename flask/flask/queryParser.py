# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 19:03:09 2020
"""
##### before first use run once: PERMISSION DENIED
#import nltk
#nltk.download('stopwords')
#####

import re
import random as rnd
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk import data
data.path.append('/usr/share/nltk_data')
from nltk.corpus import stopwords
stopWords = set(stopwords.words('english'))

rnd.seed(42)

known_operators = ('intext:','inurl:','intitle:','define:', 'translate:')

# every function in this module should take a string as the only required argument and can take optional keyword args that alter behavioour.
# every function returns an output string and a dictionary, containing at least the pair 'report':'<some string>'

def naiveParser(instring):
    instring=instring.lower()
    patterns=[r'[^\<\>]?(\".*?\")[^\<\>]', r'(\([^\<\>]*?\))',r'([^\<\>]*?)[\s$]',r'\b([a-z]*)\b']
    querySplit=[]
    for pattern in patterns:
        print('pattern: ',pattern)
        if instring.strip()!="":
            matches=re.findall(pattern,instring)
            for match in matches:
                if match!="":
                    print('match: ',match)
                    instring=instring.replace(match,"<{}>".format(len(querySplit))).strip()
                    querySplit.append(match)
    # now get rid of all trash characters between the position tags:
    trash=re.findall(r'\>(\S*?)\<',instring)
    for el in trash:
        instring=instring.replace(el," ")
    return querySplit, instring
    
def naiveAntiParser(searchTerms,posString):
    cursorInTag = False
    cache = ''
    res = ''
    for el in posString:
        if not cursorInTag and el!='<':
            res+=el
        elif not cursorInTag and el=='<':
            cursorInTag=True
        elif cursorInTag and el!='>':
            cache+=el
        elif cursorInTag and el=='>':
            res += searchTerms[int(cache)]
            cursorInTag = False
            cache = ''
        else:
            raise Exception('Error occured parsing search terms to query string:\n\t{}'.format(str((searchTerms,posString,res))))
    return res


def findStopwords(instring):
    words = word_tokenize(instring)
    wordsFiltered = []
    wordsRemoved = set()
    for word in words:
        if word not in stopWords:
            wordsFiltered.append(word)
        else: 
            wordsRemoved.add(word)
    return wordsFiltered

def dropTerm(searchTerms,posString):
    dropIndex = rnd.randint(0,len(searchTerms))
    try:
        searchTerms[dropIndex]=""
        ret=(searchTerms,posString,True)
    except:
        ret=(searchTerms,posString,False)
    return ret

def swapQuotes(searchTerms,posString):
    return searchTerms,posString,False
    

def modifyQuery(searchTerms,posString, maxretries=None):
    """ randomly picks a modification and tries to perform it on the inputs. 
    returns result on success, retries with a new pick on fail (caution:danger of infinite Loop...)"""
    counter = 0
    modSuccess=False
    mods = [dropTerm, swapQuotes]
    while counter != maxretries and not modSuccess:
        counter += 1
        mod=rnd.choice(mods)
        s,p,modSuccess = mod(searchTerms,posString)
    return s,p

