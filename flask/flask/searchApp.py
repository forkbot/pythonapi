# -*- coding: utf-8 -*-
"""
!!!! Doku hier: https://cloud.google.com/dialogflow/docs/fulfillment-webhook#webhook_response
"""

### Import section
try:
    import json
    #import logging as lg
    import sys
    from flask import Flask, request, make_response, jsonify
    from googleapiclient.discovery import build
    sys.path.insert(0, '/var/www/hmiapi/flask/flask/')
    from queryParser import naiveParser, naiveAntiParser, modifyQuery, dropTerm, swapQuotes, findStopwords 
    from userProcess import getUser, addUser
except Exception as e:
    print("Import error occured, please check dependencies!".format(e))
### End Import section


### init flask in global layout
app = Flask(__name__)
#lg.basicConfig(stream=sys.stderr)
#filename='searches.log', filemod='a')
#lg.warn('this is the final warning ;-)')
#lg.info('this is just fyi')
#lg.debug('this is debugging data')
### init cse-resource


print('reloading...')

api_key = "AIzaSyBbtU8iAGoYVhWtv4irXn28W5CcJIEZzB8"
cse_key = "007175631489998748943:w29zkedhcri"


resource = build("customsearch", "v1", developerKey=api_key).cse()
global searchCounter
searchCounter=0

@app.route('/query', methods=['GET','POST'])
def query():
    if request.method=='GET':
        query = request.args.get('query','no query')
    elif request.method=='POST':
        query = getQuery()
    s,p = naiveParser(query)
    print(s)
    print(p)
    print(findStopwords(query))
    new_s,new_p = modifyQuery(s,p,10)
    new_query = naiveAntiParser(new_s,new_p)
    return wrapResults(str([query,new_query]))


@app.route('/echo', methods=['POST','GET'])
def echo():
    res=dict()
    try:
        res['method'] = request.method
        res['headers'] = request.headers
        res['args'] = request.args
        res['json'] = request.json 
        res['form'] = request.form
        res['values'] = request.values
        res['data'] = request.data
    except Exception as e:
        res['Error']=type(e)
        res['ErrorMSG']=e
        res['request']=request
    return wrapResults(res)

@app.route('/search', methods=['GET','POST'])
def search():
    query = getQuery()
    results = doSearch(query)
    #results["searchCount"]=searchCounter
    response = wrapResults(results)
    return response

def getQuery():
    if request.method == "POST":
        req_body = json.loads(str(request.data,'utf-8'))
        try:
            query = req_body['query']
        except TypeError as e:
            print(req_body, type(req_body))
            print(request.get_json(force=True))
            raise Exception('no query string'+str(req_body)+str(type(req_body)))
    else:
        req_body = request.values
        try:
            query = req_body['query']
        except Exception as e:
            print(req_body,str(request))
            raise Exception('no query string'+str(req_body)+str(type(req_body)))      
    return query

def doSearch(query_string):
    result = resource.list(q=query_string, cx=cse_key).execute()
    #searchCounter+=1
    return result   

def wrapResults(res):
    res = json.dumps(res,indent=4, default=str)
    #lg.info("here's what we're returning: \n"+res)
    r = make_response(res)  #jsonify(res))
    r.headers['Content-Type'] = 'application/json'
    return r

@app.route('/')
def hello():
    return "up and running\nendpoints: '/query' (GET|POST), '/echo' (GET|POST), '/search' (GET only)\nenjoy ;-P"

# function for responses
def results():
    # build a request object
    req = request.get_json(force=True)
    # fetch action from json
    action = req.get('queryResult').get('action')

    # return a fulfillment response
    return {'fulfillmentText': 'This is a response from webhook.'}

# create a route for userProcess-stuff
@app.route('/addUser', methods=['POST'])
def adduser():
    data = getQuery()
    print(data)
    res = addUser(*data)
    return make_response(jsonify(res))


# create a route for userProcess-stuff
@app.route('/getUser', methods=['GET'])
def getuser():
    uname = getQuery()
    res = getUser(uname)
    return make_response(jsonify(res))

# create a route for webhook
@app.route('/webhook', methods=['GET', 'POST'])
def webhook():
    # return response
    return make_response(jsonify(results()))

if __name__ == "__main__":
    app.run(debug=True)
