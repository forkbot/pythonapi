# -*- coding: utf-8 -*-
"""
Created on Fri Jul  3 10:45:03 2020

@author: danie
"""

# import, connect, get cursor
import sqlite3
db_path = "/home/daniel/test.db"
#'F:\\portableApps\\PortableApps\\SQLiteDatabaseBrowserPortable\\chattobotto.db'
#con=sqlite3.connect(db_path,timeout=10)
#curs=con.cursor()




def addUser(userID,connTime):
    """ takes strings userID and connTime as inputs and writes them to db-table 'humans'. 
            the third value in each row (disconn.Time) is set to None.
        returns rownumber on success, Error string on db errors (e.g when trying to add existing ID)"""
    insert_sql = "insert into main.humans(user_id,connected,disconnected) values(?,?,?);"
    with sqlite3.connect(db_path,timeout=10) as con:
        try:
            curs=con.cursor()
            curs.execute(insert_sql,(userID,connTime,None))
            ret = str(curs.lastrowid)
        except Exception as e:
            ret = "Error writing to db:\n"+str(e)
        finally:
            curs.close() ## only close cursor here? conn is closed by with-clause...?
    return ret


def getUser(userID):
    """ retrieves row with key 'userID' from db-table 'humans' and returns it as a (3-tuple)-string. 
            if ID is not found, return value is 'None'
        returns Error string on db errors. """
    select_sql = "select * from main.humans where user_id=?;"
    with sqlite3.connect(db_path,timeout=10) as con:
        try:
            curs=con.cursor()
            curs.execute(select_sql,(userID,))
            ret = str(curs.fetchone())
        except Exception as e:
            ret = "Error reading from db:\n"+str(e)
        finally:
            curs.close() ## only close cursor here? conn is closed by with-clause...?
    return ret

    
if __name__ == '__main__':
    # some tests
    print(addUser('upti','wupti'))
    print(getUser('test'))
    print(getUser('upti'))
